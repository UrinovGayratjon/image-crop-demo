package uz.xsoft.imagecrop

import android.R.attr.maxHeight
import android.R.attr.maxWidth
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AppCompatActivity
import com.yalantis.ucrop.UCrop
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File


class MainActivity : AppCompatActivity() {
    private val PICK_IMAGE_GALLERY_REQUEST_CODE = 4
    private val CAMERA_ACTION_PICK_REQUEST_CODE = 5
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buttonChoiceImage.setOnClickListener {
            openImagesDocument()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == UCrop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            val uri = UCrop.getOutput(data!!)
            showImageResult(uri)
        } else if (requestCode == PICK_IMAGE_GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val sourceUri = data.data
            val file: File = getImageFile()
            val destinationUri = Uri.fromFile(file)
            showImageOriginal(sourceUri)
            openCropActivity(sourceUri!!, destinationUri)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun openCropActivity(
        sourceUri: Uri,
        destinationUri: Uri
    ) {
        UCrop.of(sourceUri, destinationUri)
            .withMaxResultSize(maxWidth, maxHeight)
            .withAspectRatio(5f, 5f)
            .start(this)
    }

    private fun openImagesDocument() {
        val pictureIntent = Intent(Intent.ACTION_GET_CONTENT)
        pictureIntent.type = "image/*"
        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE)
        val mimeTypes =
            arrayOf("image/jpeg", "image/png")
        pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)

        startActivityForResult(
            Intent.createChooser(pictureIntent, "Select Picture"),
            PICK_IMAGE_GALLERY_REQUEST_CODE
        )
    }

    private fun getImageFile(): File {
        val imageFileName = "JPEG_" + System.currentTimeMillis() + "_"
        val storageDir = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM
            ), "Camera"
        )
        val file = File.createTempFile(
            imageFileName, ".jpg", storageDir
        )
        return file
    }

    private fun showImageResult(uri: Uri?) {
        imageViewResult.setImageURI(uri)
    }

    private fun showImageOriginal(uri: Uri?) {
        imageViewOriginal.setImageURI(uri)
    }
}
