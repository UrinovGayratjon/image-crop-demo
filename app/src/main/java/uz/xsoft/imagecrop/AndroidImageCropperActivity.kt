package uz.xsoft.imagecrop

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_android_image_cropper.*


class AndroidImageCropperActivity : AppCompatActivity() {

    private val PICK_IMAGE_GALLERY_REQUEST_CODE = 4
    private val CAMERA_ACTION_PICK_REQUEST_CODE = 5
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_android_image_cropper)
        buttonChoiceImage.setOnClickListener {
            openImagesDocument()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE ) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                showImageResult(result.uri)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
            }
        } else if (requestCode == PICK_IMAGE_GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val sourceUri = data.data
            showImageOriginal(sourceUri)
            openCropActivity(sourceUri!!)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun openCropActivity(
        sourceUri: Uri
    ) {
        CropImage.activity(sourceUri)
            .start(this);

    }

    private fun openImagesDocument() {
        val pictureIntent = Intent(Intent.ACTION_GET_CONTENT)
        pictureIntent.type = "image/*"
        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE)
        val mimeTypes =
            arrayOf("image/jpeg", "image/png")
        pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)

        startActivityForResult(
            Intent.createChooser(pictureIntent, "Select Picture"),
            PICK_IMAGE_GALLERY_REQUEST_CODE
        )
    }

    private fun showImageResult(uri: Uri?) {
        imageViewResult.setImageURI(uri)
    }

    private fun showImageOriginal(uri: Uri?) {
        imageViewOriginal.setImageURI(uri)
    }
}
