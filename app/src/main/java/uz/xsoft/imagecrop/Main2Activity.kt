package uz.xsoft.imagecrop

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AppCompatActivity
import com.steelkiwi.cropiwa.config.CropIwaSaveConfig
import kotlinx.android.synthetic.main.activity_main2.*
import java.io.File


class Main2Activity : AppCompatActivity() {
    private val PICK_IMAGE_GALLERY_REQUEST_CODE = 4
    private val CAMERA_ACTION_PICK_REQUEST_CODE = 5
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        buttonOpen.setOnClickListener {
            openImagesDocument()
        }
        val file: File = getImageFile()
        val destinationUri = Uri.fromFile(file)
        cropView.crop(
            CropIwaSaveConfig.Builder(destinationUri)
                .setCompressFormat(Bitmap.CompressFormat.PNG)
                .setSize(
                    500,
                    500
                )
                .setQuality(100)
                .build()
        )
        cropView.setCropSaveCompleteListener { bitmapUri -> }

        cropView.setErrorListener { error -> }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE_GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val sourceUri = data.data
            showImageOriginal(sourceUri)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun openImagesDocument() {
        val pictureIntent = Intent(Intent.ACTION_GET_CONTENT)
        pictureIntent.type = "image/*"
        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE)
        val mimeTypes =
            arrayOf("image/jpeg", "image/png")
        pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)

        startActivityForResult(
            Intent.createChooser(pictureIntent, "Select Picture"),
            PICK_IMAGE_GALLERY_REQUEST_CODE
        )
    }

    private fun showImageOriginal(uri: Uri?) {
        cropView.setImageUri(uri)
    }
    private fun getImageFile(): File {
        val imageFileName = "JPEG_" + System.currentTimeMillis() + "_"
        val storageDir = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM
            ), "Camera"
        )
        val file = File.createTempFile(
            imageFileName, ".jpg", storageDir
        )
        return file
    }

}
