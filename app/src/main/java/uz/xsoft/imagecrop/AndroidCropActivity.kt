package uz.xsoft.imagecrop

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.soundcloud.android.crop.Crop
import kotlinx.android.synthetic.main.activity_android_crop.*
import java.io.File

class AndroidCropActivity : AppCompatActivity() {
    private val PICK_IMAGE_GALLERY_REQUEST_CODE = 4
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_android_crop)
        buttonChoiceImage.setOnClickListener {
            openImagesDocument()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            showImageResult(Crop.getOutput(data))
        } else if (requestCode == PICK_IMAGE_GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val sourceUri = data.data
            showImageOriginal(sourceUri)
            val destination =
                Uri.fromFile(File(cacheDir, "cropped"))
            openCropActivity(sourceUri!!, destination)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun openCropActivity(
        inputUri: Uri, outputUri: Uri
    ) {
        Crop.of(inputUri, outputUri).asSquare().start(this)
    }

    private fun openImagesDocument() {
        val pictureIntent = Intent(Intent.ACTION_GET_CONTENT)
        pictureIntent.type = "image/*"
        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE)
        val mimeTypes =
            arrayOf("image/jpeg", "image/png")
        pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)

        startActivityForResult(
            Intent.createChooser(pictureIntent, "Select Picture"),
            PICK_IMAGE_GALLERY_REQUEST_CODE
        )
    }

    private fun showImageResult(uri: Uri?) {
        imageViewResult.setImageURI(uri)
    }

    private fun showImageOriginal(uri: Uri?) {
        imageViewOriginal.setImageURI(uri)
    }
}
